{
  description = "An experimental make wrapper";

  # The input of the flake
  inputs = {
    nixpkgs.url = "nixpkgs/nixos-unstable";
    flake-utils.url = "github:numtide/flake-utils";
  };

  # The output of the flake
  outputs = inputs@{ self, nixpkgs, flake-utils }:

  flake-utils.lib.eachDefaultSystem (system:
    let
      pkgs = nixpkgs.legacyPackages.${system};
    in {
      # devShell is part of the output schema
      # mkShell is the "function" that build your shell
      devShell = pkgs.mkShell {
        buildInputs = with pkgs; [
          gnumake
          git
          coreutils
          bash
        ];
        shellHook = ''
          export MAKE_PATH="${pkgs.gnumake}/bin/make"
        '';
      };

      packages.default =
      let
        name = "m";
        #m-src = builtins.readFile ./m;
        #script = (pkgs.writeScriptBin name m-src).overrideAttrs(old: {
        #  buildCommand = "${old.buildCommand}\n patchShebangs $out";
        #});
        in pkgs.stdenv.mkDerivation {
          name = name;
          version = "0.2" ;

          src = ./.;

          nativeBuildInputs = [pkgs.makeWrapper];
          dontBuild = true;

          installPhase = ''
            mkdir -p $out/bin
            cp ${name} $out/bin
            chmod +x $out/bin/${name}

            wrapProgram $out/bin/${name} \
               --prefix PATH : ${pkgs.coreutils}/bin \
               --set MAKE_PATH ${pkgs.gnumake}/bin/make
          '';
      };
    });
}

#!/usr/bin/env bash

# An experimental make wrapper

set -euo pipefail
IFS=$'\n\t'

if [[ "${DEBUG:-}" == 1 ]]; then
  set -x
fi

find-real-make() {
  if test -f "$MAKE_PATH"; then
    echo "$MAKE_PATH"
  else
    # 1. whereis lists all the command that match `make`. Does not work well
    #    with brew
    # 2. The grep command filter out any entry that match our path
    # 3. We only keep the first element of the list
    whereis make | grep -v "$0" | head -n1
  fi
}

# Dirty but easy
find-makefile() {
  if test -f Makefile; then
    realpath Makefile
  elif test -f ../Makefile; then
    realpath ../Makefile
  elif test -f ../../Makefile; then
    realpath ../../Makefile
  elif test -f ../../../Makefile; then
    realpath ../../../Makefile
  elif test -f ../../../../Makefile; then
    realpath ../../../../Makefile
  else
    echo "No Makefile around" >&2
    exit 1
  fi
}

makefile() {
  find-makefile
}

list-target() {
  local makepath="$1"
  grep "^\w" "$makepath" | grep : | grep -v = | grep -v "^\." | cut -f 1 -d:
}

launch-make() {
  export MWRAPPED=1
  "$(find-real-make)" -C "$(dirname "$(find-makefile)")" "$@"
}

check-for-passthrough() {
  local target="${1:-}"
  local command
  command="$(extract-passthrough)"

  if [[ "$target" == "$command" ]]; then
    return 0
  else
    return 1
  fi
}

extract-passthrough() {
  "$(find-real-make)" --just-print .PASSTHROUGH
}

execute-passthrough() {
  local target="${1:-}"
  shift 1

  "$target" "$@"
}

get-passthrough-hook-content() {
  export MWRAPPED=1
  "$(find-real-make)" --just-print m-passthrough-pre-hook || true
}

main() {
  local target="${1:-}"

  if check-for-passthrough "$target"; then
    shift 1

    echo "Found .PASSTHROUGH command, executing it"

    eval "$(get-passthrough-hook-content)"
    execute-passthrough "$target" "$@"
  else
    launch-make "$@"
  fi
}

main "$@"
